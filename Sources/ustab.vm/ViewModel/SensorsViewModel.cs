﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using ustab.utils.Chart;
using ustab.utils.Interfaces;

namespace ustab.vm.ViewModel
{
    public class SensorsViewModel : ViewModelBase, IDatagramConsumer
    {
        #region Constants

        public readonly int MaxPointsCount = 300;

        #endregion // Constants

        #region Fields

        private readonly EnumerableDataSource<Point> _yawSource;

        private readonly EnumerableDataSource<Point> _pitchSource;

        private readonly EnumerableDataSource<Point> _rollSource;

        private List<Point> _yawData;
        private List<Point> _pitchData;
        private List<Point> _rollData;
        private Random _rnd;
        private ICommand _addTestDataCommand;
        private bool _isUdpListeningEnabled;

        #endregion // Fields

        #region Properties

        public bool IsUdpListeningEnabled 
        {
            get { return _isUdpListeningEnabled; }
            set 
            {
                if (value)
                {
                    _yawData.Clear(); _yawSource.RaiseDataChanged();
                    _pitchData.Clear(); _pitchSource.RaiseDataChanged();
                    _rollData.Clear(); _rollSource.RaiseDataChanged();
                }

                Set(ref _isUdpListeningEnabled, value); 
            }
        }
        public EnumerableDataSource<Point> YawSource
        {
            get { return _yawSource; }
        }

        public EnumerableDataSource<Point> PitchSource
        {
            get { return _pitchSource; }
        }

        public EnumerableDataSource<Point> RollSource
        {
            get { return _rollSource; }
        }

        #endregion // Properties

        #region Commands

        public ICommand AddTestDataCommand 
        {
            get
            {
                return _addTestDataCommand ?? (_addTestDataCommand = new RelayCommand(() => {
                    _yawData.Add(new Point(_yawData.Count, _rnd.NextDouble()));
                    _pitchData.Add(new Point(_pitchData.Count, _rnd.NextDouble()));
                    _rollData.Add(new Point(_rollData.Count, _rnd.NextDouble()));

                    _yawSource.RaiseDataChanged();
                    _pitchSource.RaiseDataChanged();
                    _rollSource.RaiseDataChanged();
                }));
            } 
        }

        #endregion // Commands

        #region .ctors

        public SensorsViewModel(bool createFakeData = false)
        {
            _rnd = new Random(DateTime.Now.Millisecond);

            _yawData = new List<Point>();
            _pitchData =  new List<Point>();
            _rollData = new  List<Point>();

            if (createFakeData)
	        {
                for (int i = 0; i < 100; i++)
                {
                    _yawData.Add(new Point(i, _rnd.NextDouble()));
                    _pitchData.Add(new Point(i, _rnd.NextDouble()));
                    _rollData.Add(new Point(i, _rnd.NextDouble()));
                }
	        }
            
            _yawSource = new EnumerableDataSource<Point>(_yawData);
            _yawSource.SetXMapping(p => p.X);
            _yawSource.SetYMapping(p => p.Y);

            _pitchSource = new EnumerableDataSource<Point>(_pitchData);
            _pitchSource.SetXMapping(p => p.X);
            _pitchSource.SetYMapping(p => p.Y);

            _rollSource = new EnumerableDataSource<Point>(_rollData);
            _rollSource.SetXMapping(p => p.X);
            _rollSource.SetYMapping(p => p.Y);

        }

        #endregion // .ctors

        #region IDatagramConsumer Members

        private static readonly string _gyrosPattern = @"t\:(\d+?);g\:(\d+?);(\d+?);(\d+?);(\d+)";
        private static readonly Regex _gyrosRegex = new Regex(_gyrosPattern);

        public void Process(string datagram)
        {
            var match = _gyrosRegex.Match(datagram);

            if (match.Success)
            {
                var time = int.Parse(match.Groups[1].Value);
                var g1 = int.Parse(match.Groups[2].Value);
                var g2 = int.Parse(match.Groups[3].Value);
                var g3 = int.Parse(match.Groups[4].Value);

                _yawData.Add(new Point(time, g1));
                _pitchData.Add(new Point(time, g2));
                _rollData.Add(new Point(time, g3));

                if (_yawData.Count > MaxPointsCount)
                {
                    _yawData.RemoveAt(0);
                    _pitchData.RemoveAt(0);
                    _rollData.RemoveAt(0);
                }

                _yawSource.RaiseDataChanged();
                _pitchSource.RaiseDataChanged();
                _rollSource.RaiseDataChanged();
            }
        }

        #endregion
    }
}
