using GalaSoft.MvvmLight;

namespace ustab.vm.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        #region Properties

        public SensorsViewModel SensorsData { get; private set; }

        #endregion

        #region .ctors

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                SensorsData = new SensorsViewModel(true);
            }
            else
            {
                SensorsData = new SensorsViewModel(true);
            }
        }

        #endregion
    }
}