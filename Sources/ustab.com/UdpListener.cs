﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using ustab.utils.Interfaces;

namespace ustab.com
{
    public class UdpListener : Behavior<FrameworkElement>
    {
        #region Fields

        private UdpClient _client;

        #endregion

        #region Properties

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.Register("IsEnabled", typeof(bool), typeof(UdpListener), new PropertyMetadata(false, OnIsEnabledChanged));

        public string RemoteIpAddress
        {
            get { return (string)GetValue(RemoteIpAddressProperty); }
            set { SetValue(RemoteIpAddressProperty, value); }
        }

        public static readonly DependencyProperty RemoteIpAddressProperty =
            DependencyProperty.Register("RemoteIpAddress", typeof(string), typeof(UdpListener), new PropertyMetadata("127.0.0.1"));


        public int Port
        {
            get { return (int)GetValue(PortProperty); }
            set { SetValue(PortProperty, value); }
        }


        public static readonly DependencyProperty PortProperty =
            DependencyProperty.Register("Port", typeof(int), typeof(UdpListener), new PropertyMetadata(8899));


        public IDatagramConsumer DatagramProcessor
        {
            get { return (IDatagramConsumer)GetValue(DatagramProcessorProperty); }
            set { SetValue(DatagramProcessorProperty, value); }
        }

        public static readonly DependencyProperty DatagramProcessorProperty =
            DependencyProperty.Register("DatagramProcessor", typeof(IDatagramConsumer), typeof(UdpListener), new PropertyMetadata(null));
        
        #endregion // Properties

        #region Private Methods

        void UpdateClient()
        {
            _client = null;

            try
            {
                _client = new UdpClient(Port, AddressFamily.InterNetwork);
                // FIXME: Use Logger here
                //Console.WriteLine("\nClient for UDP server at {0}:{1} created.\n", ipString, port);
            }
            catch (Exception ex)
            {
                // FIXME: Use Logger here
                //Console.WriteLine(string.Format("Unable to parse the IP Address specified.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }
        
        private async Task Terminate()
        {
            try
            {
                var ipAddress = IPAddress.Parse("127.0.0.1");
                var endPoint = new IPEndPoint(ipAddress, Port);

                var data = UTF8Encoding.UTF8.GetBytes("+++terminate+++");
                var sentBytes = await _client.SendAsync(data, data.Length, endPoint);
            }
            catch (Exception ex)
            {
                // FIXME: Use Logger here
                //Console.WriteLine(string.Format("Unable to parse the IP Address specified.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private async Task Listening()
        {
            try
            {
                var ipAddress = IPAddress.Parse(RemoteIpAddress);
                var endPoint = new IPEndPoint(ipAddress, Port);

                while (IsEnabled)
                {
                    var result = await Task.Run(() => {
                        _client.Send(Encoding.ASCII.GetBytes("\"(\r\n"), 4, endPoint);

                        return _client.Receive(ref endPoint);
                    });
                    var stringResult = Encoding.UTF8.GetString(result);

                    if (DatagramProcessor != null)
                    {
                        DatagramProcessor.Process(stringResult);
                    }
                }

                if (_client != null)
                {
                    _client.Close();
                }
            }
            catch (Exception ex)
            {
                // FIXME: Use Logger here
                //Console.WriteLine(string.Format("Unable to parse the IP Address specified.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        #endregion // Private Methods

        #region Event Handlers

        private static void OnIsEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (UdpListener)d;

            if (e.NewValue.Equals(true))
            {
                behavior.UpdateClient();
                
                // FIXME: It should be awaited or reworked in future
                behavior.Listening();
            }
            else
            {
                // FIXME: It should be awaited or reworked in future
                behavior.Terminate();
            }
        }

        #endregion // Event Handlers

    }
}
