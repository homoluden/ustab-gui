﻿using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;

namespace ustab.client.Behaviors
{
    public class AttachedLineChart : Behavior<ChartPlotter>
    {
        private LineGraph _lineChart;

        public EnumerableDataSource<ustab.utils.Chart.Point> Points
        {
            get { return (EnumerableDataSource<ustab.utils.Chart.Point>)GetValue(PointsProperty); }
            set { SetValue(PointsProperty, value); }
        }

        public static readonly DependencyProperty PointsProperty =
            DependencyProperty.Register("Points", typeof(EnumerableDataSource<ustab.utils.Chart.Point>), typeof(AttachedLineChart), new PropertyMetadata(null, OnPointsChanged));


        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(Color), typeof(AttachedLineChart), new PropertyMetadata(Colors.IndianRed));


        public double Thickness
        {
            get { return (double)GetValue(ThicknessProperty); }
            set { SetValue(ThicknessProperty, value); }
        }

        public static readonly DependencyProperty ThicknessProperty =
            DependencyProperty.Register("Thickness", typeof(double), typeof(AttachedLineChart), new PropertyMetadata(1.0));

        
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(AttachedLineChart), new PropertyMetadata("Attached Line"));

        

        protected override void OnAttached()
        {
            base.OnAttached();
        }

        static void OnPointsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (AttachedLineChart)d;

            if (e.NewValue != null)
            {
                if (behavior._lineChart != null)
                {
                    behavior._lineChart.Remove();
                }
                
                behavior._lineChart = behavior.AssociatedObject.AddLineGraph(behavior.Points, behavior.Color, behavior.Thickness, behavior.Description);
            }
        }
    }
}
