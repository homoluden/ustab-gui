﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ustab.utils.Interfaces
{
    public interface IDatagramConsumer
    {
        void Process(string datagramm);
    }
}
